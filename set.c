#include "set.h"
#include <stdio.h>
#include <stdlib.h>

// Help functions //
// -------------- //

/**
 * Compare two integers
 *
 * Note: the purpose of this function is to be used with bsearch and qsort of
 * the stdlib library.
 *
 * @param i1  A pointer to the first integer
 * @param i2  A pointer to the second integer
 * @return    0  if *i1 == *i2
 *            <0 if *i1 < *i2
 *            >0 if *i1 > *i2
 */
int compare_ints(const void *i1, const void *i2) {
    return *(int*)i1 - *(int*)i2;
}

// Functions //
// --------- //

struct set *set_create(void) {
    struct set *set = malloc(sizeof(struct set));
    set->elements = malloc(sizeof(int));
    set->capacity = 1;
    set->size = 0;
    return set;
}

void set_delete(struct set *set) {
    free(set->elements);
    free(set);
}

bool set_is_empty(const struct set *set) {
    return set->size == 0;
}

bool set_contains(const struct set *set,
                  int element) {
    return bsearch(&element, set->elements, set->size,
                   sizeof(int), compare_ints) != NULL;
}

void set_add(struct set *set,
             int element) {
    unsigned int i = 0;
    while (i < set->size && set->elements[i] < element)
        ++i;
    if (set->elements[i] == element) return;
    if (set->size == set->capacity) {
        set->capacity *= 2;
        set->elements = realloc(set->elements, set->capacity * sizeof(int));
    }
    for (unsigned int j = set->size; j > i; --j)
        set->elements[j] = set->elements[j - 1];
    set->elements[i] = element;
    ++set->size;
}

void set_remove(struct set *set,
                int element) {
    if (!set_contains(set, element)) return;
    unsigned int i = 0;
    while (i < set->size && set->elements[i] != element)
        ++i;
    while (i < set->size - 1) {
        set->elements[i] = set->elements[i + 1];
        ++i;
    }
    --set->size;
}

void set_print(const struct set *set) {
    printf("{");
    for (unsigned int i = 0; i < set->size; ++i) {
        if (i > 0) printf(", ");
        printf("%d", set->elements[i]);
    }
    printf("}");
}
