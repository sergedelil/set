CFLAGS = -Wall -Wextra
LFLAGS = -ltap

test_set: test_set.o set.o
	gcc test_set.o set.o $(LFLAGS) -o test_set

set.o: set.h set.c
	gcc -c $(CFLAGS) set.c

test_set.o: test_set.c
	gcc -c $(CFLAGS) test_set.c

.PHONY: clean test

clean:
	rm -f *.o
	rm -f test_set

test: test_set
	./test_set
