#ifndef SET_H
#define SET_H

#include <stdbool.h>

// Types //
// ----- //

// A set of integers
struct set {
    int *elements;         // Its elements
    unsigned int size;     // Its cardinality
    unsigned int capacity; // Its capacity
};

// Functions //
// --------- //

/**
 * Create an empty set
 *
 * @return  An empty set
 */
struct set *set_create(void);

/**
 * Delete a set
 *
 * @param set  The set to delete
 */
void set_delete(struct set *set);

/**
 * Check if a set is empty
 *
 * @param set  The set to check
 * @return     true if the set is empty
 *             false otherwise
 */
bool set_is_empty(const struct set *set);

/**
 * Check if a set contains an element
 *
 * @param set      The set
 * @param element  The element
 * @return         true if the set contains the element
 *                 false otherwise
 */
bool set_contains(const struct set *set,
                  int element);

/**
 * Add an element to a set
 *
 * Note: If the element already belongs to the set, then the set remains
 * unchanged.
 *
 * @param set      The set
 * @param element  The element to add
 */
void set_add(struct set *set,
             int element);

/**
 * Remove an element from a set
 *
 * Note: If the element already belongs to the set, then the set remains
 * unchanged.
 *
 * @param set      The set
 * @param element  The element to remove
 */
void set_remove(struct set *set,
                int element);

/**
 * Print a set to stdout
 *
 * @param  The set to print
 */
void set_print(const struct set *set);

#endif
