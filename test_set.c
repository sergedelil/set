#include "set.h"
#include <tap.h>

int main(void) {
    struct set *set = set_create();
    ok(set_is_empty(set), "created set is empty");
    ok(set->size == 0, "size of set is 0");
    set_add(set, 3);
    set_add(set, 5);
    set_add(set, 2);
    diag("Adding 3, 5, 2");
    printf("# set = "); set_print(set); printf("\n");
    ok(set->size == 3, "size of set is 3");
    ok(set_contains(set, 3), "set contains 3");
    ok(!set_contains(set, 1), "set does not contains 1");
    diag("Adding 5 again");
    set_add(set, 5);
    printf("# set = "); set_print(set); printf("\n");
    ok(set->size == 3, "size of set is still 3");
    diag("Removing 8");
    set_remove(set, 8);
    printf("# set = "); set_print(set); printf("\n");
    ok(set->size == 3, "size of set is still 3");
    diag("Removing 3");
    set_remove(set, 3);
    printf("# set = "); set_print(set); printf("\n");
    ok(!set_contains(set, 3), "set does not contains 3 anymore");
    set_delete(set);
    done_testing();
}
